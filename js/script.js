
let parent = document.querySelector(".images-wrapper");
let active = document.querySelector(".active");

const countImg = document.getElementsByTagName("img").length

let btnStop = document.getElementById("stop");
let btnStart = document.getElementById("start");

function Child() {
   document.querySelectorAll(".active").forEach(item => {
      item.classList.remove("active");
   });
   let next = active.nextElementSibling;
   if(next === null){
      next = parent.children[0];
   }
   next.classList.add("active");
   active = next;
}

let i = 0;

function start() {
  let interval = setInterval(() => {
    i++;
    if (i === countImg) {
      document.querySelectorAll(".button").forEach(item => {
        item.classList.add("show");
      });
    }
    Child()
    btnStop.addEventListener("click", function () {
      clearInterval(interval);
    });
  }, 3000);
}

start()

btnStart.addEventListener("click", function () {
  start()
});


